﻿using System.Windows;
using System.Windows.Input;

using CefSharp.Wpf;

namespace EnterpriseManagement.Client
{
    public partial class HelpDialog
    {
        public HelpDialog()
        {
            InitializeComponent();
        }

        public string Hyperlink
        { get; set; }

        private void HelpDialog_OnLoaded(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;
            var browser = new ChromiumWebBrowser();
            browser.NavStateChanged += (s, navigationArgs) =>
            {
                if (!navigationArgs.IsLoading)
                {
                    Dispatcher.Invoke(() => { Cursor = Cursors.Arrow; });
                }
            };
            dockPanel.Children.Add(browser);
            browser.Address = Hyperlink;
        }
    }
}
