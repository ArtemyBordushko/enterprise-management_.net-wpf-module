# README #


### OSTIS Enterprise Management System - .NET WPF Module ###

Демо-версия модуля графического пользовательского интерфейса

### Инструкция ###
Для запуска необходимо:

1) Иметь сеть из хотя бы двух компьютеров с ОС WIndows на одном и ОС Ubuntu на другом (либо виртуальная машина с ОС Ubuntu на компьютере с ОС Windows)


2) Иметь на компьютере (или ВМ) с Ubuntu установленную последнюю версию системы OSTIS, установить которую можно по следующему алгоритму:


   1. Установить Git на Ubuntu


   2. Через терминал клонировать репозиторий, расположенный по адресу "http://github.com/ShunkevichDV/ostis" (git clone http://github.com/ShunkevichDV/ostis)


   3. Из локальной копии репозитория из папки ostis/scripts (перейти в неё можно командой из терминала "cd ostis/scripts") запустить скрипт prepare.sh (в терминале ввести команду: "./prepare.sh")


   4. После завершения работы скрипта необходимо собрать SCP-машину. Для этого нужно из папки ostis/scp-machine/scripts запустить скрипт "make_all.sh" ("./make_all.sh")


   5. Теперь необходимо поместить исходные тексты базы знаний системы. Для этого в папку ostis/kb клонировать репозиторий, адрес которого - "https://github.com/ArtemyBordushko/enterprise-management-system" (в терминале прописать команду "git clone http://github.com/ArtemyBordushko/enterprise-management-system")


   6. После клонирования исходных текстов БЗ необходимо собрать БЗ и запустить систему. Для этого в терминале необходимо перейти в папку "ostis/scripts" и запустить скрипт "restart_sctp.sh" ("./restart_sctp.sh"). Затем открыть новое окно терминала, и в нём из из той же папки запустить скрипт "./run_scweb.sh" ("./run_scweb.sh")


3) Под ОС Windows клонировать данный репозиторий.


4) Теперь под ОС Windows необходимо создать базу данных. Для этого необходимо иметь установленный Microsoft SQL Server. Для создания БД нужно запустить скрипты через Microsoft SQL Server из папки DatabaseScripts (сначала скрипт "0001. Create database.sql", а затем "0002. Create consistency triggers.sql")


5) В Visual Studio запустить "EnterpriseManagement.sln".

6) В файле App.config проекта "EnterpriseManagement.Client" необходимо атрибуту "hyperlink" тега "ostis" присвоить адрес IP inet соединения в Ubuntu с портом 8000 (например: <ostis hyperlink="http://192.168.37.192:8000" />).  Адрес можно узнать, прописав в терминале команду "ifconfig".

### Контакты ###

* artemy.bordushko@gmail.com