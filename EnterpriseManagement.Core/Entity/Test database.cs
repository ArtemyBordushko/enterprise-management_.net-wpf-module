﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EnterpriseManagement.Core.Entity
{
    partial class EnterpriseManagementEntities
    {
        public static void CreateTestBase(EnterpriseManagementEntities database)
        {
            var random = new Random(DateTime.Now.Millisecond);

            // добавление должностей
            Position positionHead, positionCleaner, positionWorker;
            database.AddToPosition(positionHead = new Position { Name = "Генеральный директор" });
            database.AddToPosition(positionCleaner = new Position { Name = "Клининг-менеджер" });
            database.AddToPosition(positionWorker = new Position { Name = "Рабочий-многостаночник" });

            // добавление групп продукции
            ProductGroup groupHomeElectronics, subGroupTvs, subGroupFridges, groupBeverages, subGroupAlcohol, subGroupNonAlcohol,
            groupRoot = database.ProductGroup.Single();

            database.AddToProductGroup(groupHomeElectronics = new ProductGroup
            {
                Name = "Бытовая техника",
                Parent = groupRoot,
            });
            database.AddToProductGroup(groupBeverages = new ProductGroup
            {
                Name = "Напитки",
                Parent = groupRoot,
            });
            groupHomeElectronics.Children.Add(subGroupTvs = new ProductGroup
            {
                Name = "Телевизоры",
                Parent = groupHomeElectronics,
            });
            groupHomeElectronics.Children.Add(subGroupFridges = new ProductGroup
            {
                Name = "Холодильники",
                Parent = groupHomeElectronics,
            });
            groupBeverages.Children.Add(subGroupAlcohol = new ProductGroup
            {
                Name = "Спиртные напитки",
                Parent = groupBeverages,
            });
            groupBeverages.Children.Add(subGroupNonAlcohol = new ProductGroup
            {
                Name = "Безалкогольные напитки",
                Parent = groupBeverages,
            });
            
            Property propertyElectricity, propertyLitrage, propertyTvPrograms, propertyFridgeFreeze, propertyAlcoholeLevel, propertyHasJuice;
            groupHomeElectronics.Properties.Add(propertyElectricity = new Property
            {
                Name = "Рабочее напряжение, В",
                PropertyTypeID = (byte) PropertyTypeEnum.Integer,
                IsRequired = false,
            });
            groupBeverages.Properties.Add(propertyLitrage = new Property
            {
                Name = "Литров в бутылке",
                PropertyTypeID = (byte) PropertyTypeEnum.Decimal,
                IsRequired = true,
            });
            subGroupTvs.Properties.Add(propertyTvPrograms = new Property
            {
                Name = "Количество ТВ-программ",
                PropertyTypeID = (byte) PropertyTypeEnum.Integer,
                IsRequired = true,
            });
            subGroupFridges.Properties.Add(propertyFridgeFreeze = new Property
            {
                Name = "Минимальная температура в морозилке",
                PropertyTypeID = (byte) PropertyTypeEnum.Integer,
                IsRequired = true,
            });
            subGroupAlcohol.Properties.Add(propertyAlcoholeLevel = new Property
            {
                Name = "Градусов алкоголя",
                PropertyTypeID = (byte) PropertyTypeEnum.Decimal,
                IsRequired = true,
            });
            subGroupNonAlcohol.Properties.Add(propertyHasJuice = new Property
            {
                Name = "Содержит сок",
                PropertyTypeID = (byte) PropertyTypeEnum.Boolean,
                IsRequired = true,
            });
            
            // добавление типов документов
            DocumentType tnType, ttnType, invoiceType, checkType;
            database.AddToDocumentType(tnType = new DocumentType
            {
                Name = "Товарная накладная",
            });
            database.AddToDocumentType(ttnType = new DocumentType
            {
                Name = "Товаро-транспортная накладная",
            });
            database.AddToDocumentType(invoiceType = new DocumentType
            {
                Name = "Счёт-фактура",
            });
            database.AddToDocumentType(checkType = new DocumentType
            {
                Name = "Товарный чек",
            });
            var documentTypes = new List<DocumentType> { tnType, ttnType, invoiceType, checkType };
            
            Property documentNumber = new Property
                {
                    Name = "Номер",
                    PropertyTypeID = (byte) PropertyTypeEnum.String,
                    IsRequired = true,
                },
                documentContactor = new Property
                {
                    Name = "Контрагент",
                    PropertyTypeID = (byte) PropertyTypeEnum.String,
                    IsRequired = true,
                },
                documentDate = new Property
                {
                    Name = "Дата",
                    PropertyTypeID = (byte) PropertyTypeEnum.DateTime,
                    IsRequired = true,
                },
                documentVehicle = new Property
                {
                    Name = "Транспорт",
                    PropertyTypeID = (byte) PropertyTypeEnum.String,
                    IsRequired = true,
                };
            foreach (var type in documentTypes)
            {
                type.Properties.Add(documentNumber);
                type.Properties.Add(documentDate);
            }
            tnType.Properties.Add(documentContactor);
            ttnType.Properties.Add(documentContactor);
            invoiceType.Properties.Add(documentContactor);
            ttnType.Properties.Add(documentVehicle);

            // добавление реквизитов предприятия
            Property reqName, reqAddress;
            database.AddToProperty(reqName = new Property
            {
                Name = "Наименование",
                PropertyTypeID = (byte) PropertyTypeEnum.String,
                IsRequired = true,
            });
            database.AddToProperty(reqAddress = new Property
            {
                Name = "Юридический адрес",
                PropertyTypeID = (byte) PropertyTypeEnum.String,
                IsRequired = true,
            });
            database.AddToContractorDetail(new ContractorDetail { Property = reqName });
            database.AddToContractorDetail(new ContractorDetail { Property = reqAddress });

            // единицы измерения
            var allUnits = database.Unit.ToList();
            Unit unitInstance = allUnits[0],
                 //unitM2 = allUnits[1],
                 //unitKg = allUnits[2],
                 unitL = allUnits[3];

            // добавление предприятий
            Enterprise enterpriseMain, enterprise1, enterprise2, enterprise3, enterprise4;
            database.AddToEnterprise(enterpriseMain = new Enterprise { IsActive = true });
            database.AddToEnterprise(enterprise1 = new Enterprise());
            database.AddToEnterprise(enterprise2 = new Enterprise());
            database.AddToEnterprise(enterprise3 = new Enterprise());
            database.AddToEnterprise(enterprise4 = new Enterprise());

            enterpriseMain.Properties.Add(new PropertyValue
            {
                Property = reqName,
                ValueString = new PropertyValueString { Value = "Рога и копыта" },
            });
            enterpriseMain.Properties.Add(new PropertyValue
            {
                Property = reqAddress,
                ValueString = new PropertyValueString { Value = "Мой адрес - Советский Союз" },
            });
            enterprise1.Properties.Add(new PropertyValue
            {
                Property = reqName,
                ValueString = new PropertyValueString { Value = "ООО Вектор" },
            });
            enterprise1.Properties.Add(new PropertyValue
            {
                Property = reqAddress,
                ValueString = new PropertyValueString { Value = "На Дерибасовской хорошая погода" },
            });
            enterprise2.Properties.Add(new PropertyValue
            {
                Property = reqName,
                ValueString = new PropertyValueString { Value = "НИИ Пива, Водки и Вина" },
            });
            enterprise2.Properties.Add(new PropertyValue
            {
                Property = reqAddress,
                ValueString = new PropertyValueString { Value = "ул Кривых" },
            });
            enterprise3.Properties.Add(new PropertyValue
            {
                Property = reqName,
                ValueString = new PropertyValueString { Value = "МинскХлебПром" },
            });
            enterprise3.Properties.Add(new PropertyValue
            {
                Property = reqAddress,
                ValueString = new PropertyValueString { Value = "Минск" },
            });
            enterprise4.Properties.Add(new PropertyValue
            {
                Property = reqName,
                ValueString = new PropertyValueString { Value = "Савушкин Продукт" },
            });
            enterprise4.Properties.Add(new PropertyValue
            {
                Property = reqAddress,
                ValueString = new PropertyValueString { Value = "Брестская Крепость" },
            });

            enterpriseMain.Purchasers.Add(enterprise1);
            enterpriseMain.Purchasers.Add(enterprise2);
            enterpriseMain.Purchasers.Add(enterprise3);
            enterpriseMain.Suppliers.Add(enterprise2);
            enterpriseMain.Suppliers.Add(enterprise3);
            enterpriseMain.Suppliers.Add(enterprise4);

            // добавление рабочих
            enterpriseMain.Employees.Add(new Employee
            {
                BirthDate = new DateTime(1980, 1, 1),
                FirstName = "Зицпредседатель",
                LastName = "Фунт",
                MiddleName = "",
                Position = positionHead,
            });
            enterpriseMain.Employees.Add(new Employee
            {
                BirthDate = new DateTime(1905, 12, 12),
                FirstName = "Дарья",
                LastName = "Колмогорова",
                MiddleName = "Семёновна",
                Position = positionCleaner,
            });
            enterpriseMain.Employees.Add(new Employee
            {
                BirthDate = new DateTime(1990, 1, 2),
                FirstName = "Иван",
                LastName = "Иванов",
                MiddleName = "Иванович",
                Position = positionWorker,
            });
            enterpriseMain.Employees.Add(new Employee
            {
                BirthDate = new DateTime(1991, 3, 4),
                FirstName = "Пётр",
                LastName = "Петров",
                MiddleName = "Петрович",
                Position = positionWorker,
            });
            enterpriseMain.Employees.Add(new Employee
            {
                BirthDate = new DateTime(1992, 5, 6),
                FirstName = "Сидор",
                LastName = "Сидоров",
                MiddleName = "Сидорович",
                Position = positionWorker,
            });
            
            // добавление продуктов
            var allProducts = new List<Product>
            {
                new Product
                {
                    Name = "Вода питьевая Дарида",
                    Group = subGroupNonAlcohol,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyLitrage,
                            ValueDecimal = new PropertyValueDecimal { Value = 2 },
                        },
                        new PropertyValue
                        {
                            Property = propertyHasJuice,
                            ValueBoolean = new PropertyValueBoolean { Value = false },
                        },
                    },
                },
                new Product
                {
                    Name = "Вода минеральная Дарида",
                    Group = subGroupNonAlcohol,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyLitrage,
                            ValueDecimal = new PropertyValueDecimal { Value = 2 },
                        },
                        new PropertyValue
                        {
                            Property = propertyHasJuice,
                            ValueBoolean = new PropertyValueBoolean { Value = false },
                        },
                    },
                },
                new Product
                {
                    Name = "Вода минеральная Минская-4",
                    Group = subGroupNonAlcohol,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyLitrage,
                            ValueDecimal = new PropertyValueDecimal { Value = 1 },
                        },
                        new PropertyValue
                        {
                            Property = propertyHasJuice,
                            ValueBoolean = new PropertyValueBoolean { Value = false },
                        },
                    },
                },
                new Product
                {
                    Name = "Жигулёвское",
                    Group = subGroupAlcohol,
                    Unit = unitL,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyAlcoholeLevel,
                            ValueDecimal = new PropertyValueDecimal { Value = 9 },
                        },
                    },
                },
                new Product
                {
                    Name = "Крынiца",
                    Group = subGroupAlcohol,
                    Unit = unitL,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyAlcoholeLevel,
                            ValueDecimal = new PropertyValueDecimal { Value = 9 },
                        },
                    },
                },
                new Product
                {
                    Name = "Guinness",
                    Group = subGroupAlcohol,
                    Unit = unitL,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyAlcoholeLevel,
                            ValueDecimal = new PropertyValueDecimal { Value = 10 },
                        },
                    },
                },
                new Product
                {
                    Name = "Атлант 1111",
                    Group = subGroupFridges,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyElectricity,
                            ValueInteger = new PropertyValueInteger { Value = 360 },
                        },
                        new PropertyValue
                        {
                            Property = propertyFridgeFreeze,
                            ValueInteger = new PropertyValueInteger { Value = -20 },
                        },
                    },
                },
                new Product
                {
                    Name = "LG 2222",
                    Group = subGroupFridges,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyElectricity,
                            ValueInteger = new PropertyValueInteger { Value = 220 },
                        },
                        new PropertyValue
                        {
                            Property = propertyFridgeFreeze,
                            ValueInteger = new PropertyValueInteger { Value = -10 },
                        },
                    },
                },
                new Product
                {
                    Name = "Samsung 3333",
                    Group = subGroupFridges,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyElectricity,
                            ValueInteger = new PropertyValueInteger { Value = 220 },
                        },
                        new PropertyValue
                        {
                            Property = propertyFridgeFreeze,
                            ValueInteger = new PropertyValueInteger { Value = -10 },
                        },
                    },
                },
                new Product
                {
                    Name = "Горизонт 4444",
                    Group = subGroupTvs,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyElectricity,
                            ValueInteger = new PropertyValueInteger { Value = 220 },
                        },
                        new PropertyValue
                        {
                            Property = propertyTvPrograms,
                            ValueInteger = new PropertyValueInteger { Value = 10 },
                        },
                    },
                },
                new Product
                {
                    Name = "Sony 555",
                    Group = subGroupTvs,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyElectricity,
                            ValueInteger = new PropertyValueInteger { Value = 230 },
                        },
                        new PropertyValue
                        {
                            Property = propertyTvPrograms,
                            ValueInteger = new PropertyValueInteger { Value = 20 },
                        },
                    },
                },
                new Product
                {
                    Name = "Panasonic 6666",
                    Group = subGroupTvs,
                    Unit = unitInstance,
                    Properties = {
                        new PropertyValue
                        {
                            Property = propertyElectricity,
                            ValueInteger = new PropertyValueInteger { Value = 230 },
                        },
                        new PropertyValue
                        {
                            Property = propertyTvPrograms,
                            ValueInteger = new PropertyValueInteger { Value = 30 },
                        },
                    },
                }
            };
            foreach (var product in allProducts)
            {
                database.AddToProduct(product);
                if (random.Next(0, 2) == 1)
                {
                    enterpriseMain.Products.Add(product);
                }
            }

            // добавление цен
            const int minPriceCount = 1, maxPriceCount = 3;
            const double minPrice = 999.999, maxPrice = 19999.999;
            foreach (var product in enterpriseMain.Products)
            {
                int count = random.Next(minPriceCount, maxPriceCount);
                for (int i = 0; i < count; i++)
                {
                    enterpriseMain.PriceList.Add(new Price
                    {
                        Product = product,
                        Value = minPrice + random.NextDouble() * (maxPrice - minPrice),
                    });
                }
            }

            // добавление остатков
            const int minBalance = 1, maxBalance = 10000;
            foreach (var price in enterpriseMain.PriceList)
            {
                enterpriseMain.WarehouseBalance.Add(new PricePosition
                {
                    Price = price,
                    Count = random.Next(minBalance, maxBalance),
                });
            }

            database.SaveChanges();

            // добавление документов
            const int minDocumentsCount = 5, maxDocumentsCount = 50, minPositions = 1, minCount = 1, maxCount = 100;
            int maxPositions = enterpriseMain.PriceList.Count/4;
            foreach (var documentType in database.DocumentType)
            {
                int count = random.Next(minDocumentsCount, maxDocumentsCount);
                for (int i = 0; i < count; i++)
                {
                    Document document;
                    enterpriseMain.Documents.Add(document = new Document { Type = documentType });
                    foreach (var property in documentType.Properties)
                    {
                        PropertyValue value;
                        document.Properties.Add(value = new PropertyValue { Property = property });
                        switch (property.TypeAsEnum)
                        {
                            case PropertyTypeEnum.Boolean:
                                value.ValueBoolean = new PropertyValueBoolean { Value = random.Next(0, 2) > 0 };
                                break;
                            case PropertyTypeEnum.Integer:
                                value.ValueInteger = new PropertyValueInteger { Value = random.Next() };
                                break;
                            case PropertyTypeEnum.Decimal:
                                value.ValueDecimal = new PropertyValueDecimal { Value = random.NextDouble() };
                                break;
                            case PropertyTypeEnum.String:
                                value.ValueString = new PropertyValueString { Value = "строка №" + random.NextDouble().ToString("N5") };
                                break;
                            case PropertyTypeEnum.DateTime:
                                value.ValueDateTime = new PropertyValueDateTime { Value = DateTime.Now };
                                break;
                            default:
                                throw new NotSupportedException();
                        }
                    }
                    int positions = random.Next(minPositions, maxPositions);
                    var availablePrices = enterpriseMain.PriceList.ToList();
                    for (int j = 0; j < positions; j++)
                    {
                        var price = availablePrices[random.Next(0, availablePrices.Count)];
                        document.Positions.Add(new PricePosition
                        {
                            Price = price,
                            Count = random.Next(minCount, maxCount),
                        });
                        availablePrices.Remove(price);
                    }
                }
            }

            database.SaveChanges();
        }
    }
}
