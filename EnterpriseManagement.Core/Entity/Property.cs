﻿using System;
using System.Data.Objects;

namespace EnterpriseManagement.Core.Entity
{
    partial class Property
    {
        public PropertyTypeEnum TypeAsEnum
        { get { return (PropertyTypeEnum) PropertyTypeID; } }

        public void SetType(PropertyTypeEnum type, EnterpriseManagementEntities database)
        {
            if (Enum.IsDefined(typeof (PropertyTypeEnum), type) && (type != PropertyTypeEnum.Unknown))
            {
                Type = PropertyType.Lookup(database, type, MergeOption.OverwriteChanges);
            }
            else
            {
                throw new ArgumentNullException("type");
            }
        }
    }
}
