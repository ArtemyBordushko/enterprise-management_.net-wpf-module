﻿using System.Data.Objects;

namespace EnterpriseManagement.Core.Entity
{
    partial class ProductGroup
    {
        public void LoadChildren(MergeOption mergeOption)
        {
            if (!Children.IsLoaded)
            {
                Children.Load(mergeOption);
            }
            foreach (var child in Children)
            {
                child.LoadChildren(mergeOption);
            }
        }
    }
}
