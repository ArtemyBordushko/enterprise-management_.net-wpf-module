﻿using System.Data.Objects;
using System.Linq;

namespace EnterpriseManagement.Core.Entity
{
    public enum PropertyTypeEnum : byte
    {
        Unknown = 0,
        Boolean = 1,
        Integer = 2,
        Decimal = 3,
        String = 4,
        DateTime = 5,
    }

    partial class PropertyType
    {
        public PropertyTypeEnum EnumValue
        { get { return (PropertyTypeEnum) ID; } }

        public static PropertyType Lookup(EnterpriseManagementEntities database, PropertyTypeEnum value, MergeOption mergeOption = MergeOption.NoTracking)
        {
            byte _value = (byte) value;
            return database.PropertyType.Execute(mergeOption).FirstOrDefault(pt => pt.ID == _value);
        }
    }
}
