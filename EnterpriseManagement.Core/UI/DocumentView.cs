﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Objects;
using System.Linq;

using EnterpriseManagement.Core.Entity;

namespace EnterpriseManagement.Core.UI
{
    public class DocumentView
    {
        public long Id
        { get { return id; } }

        private readonly long id;
        private readonly Dictionary<long, object> propertyValues = new Dictionary<long, object>();

        public DocumentView(Document document)
        {
            id = document.ID;
            if (!document.Properties.IsLoaded)
            {
                document.Properties.Load(MergeOption.NoTracking);
            }
            foreach (var value in document.Properties)
            {
                propertyValues[value.PropertyID] = value.RawValue;
            }
        }

        public object GetPropertyValue(long propertyId)
        {
            object result;
            return propertyValues.TryGetValue(propertyId, out result) ? result : null;
        }
    }

    public class DocumentViewTypeDescriptor : CustomTypeDescriptor
    {
        public static List<GridColumnDefinition> InitializeColumns(DocumentType documentType)
        {
            List<GridColumnDefinition> columns;
            if (!allColumns.TryGetValue(documentType.ID, out columns))
            {
                if (!documentType.Properties.IsLoaded)
                {
                    documentType.Properties.Load(MergeOption.NoTracking);
                }
                allColumns[documentType.ID] = columns = documentType.Properties.Select(p => new GridColumnDefinition(p.Name, p.ID, p.TypeAsEnum)).ToList();
            }
            return columns;
        }

        public static void InitializeProperties(long documentType)
        {
            allProperties = new PropertyDescriptorCollection(allColumns[documentType].Select(c => new DocumentViewPropertyDescriptor(c)).OfType<PropertyDescriptor>().ToArray());
        }

        private static readonly Dictionary<long, List<GridColumnDefinition>> allColumns = new Dictionary<long, List<GridColumnDefinition>>();
        private static PropertyDescriptorCollection allProperties;
        
        public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return allProperties;
        }

        public override PropertyDescriptorCollection GetProperties()
        {
            return allProperties;
        }

        static DocumentViewTypeDescriptor()
        {
            TypeDescriptor.AddProvider(new DocumentViewTypeDescriptorProvider(), typeof(DocumentView));
        }
    }

    internal class DocumentViewTypeDescriptorProvider : TypeDescriptionProvider
    {
        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            return new DocumentViewTypeDescriptor();
        }
    }

    public class DocumentViewPropertyDescriptor : PropertyDescriptor
    {
        public GridColumnDefinition Column
        { get { return column; } }

        private readonly GridColumnDefinition column;

        public DocumentViewPropertyDescriptor(GridColumnDefinition column)
            : base(column.Header, new Attribute[0])
        {
            this.column = column;
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override object GetValue(object component)
        {
            return ((DocumentView) component).GetPropertyValue(column.PropertyId);
        }

        public override void ResetValue(object component)
        {
            throw new NotSupportedException();
        }

        public override void SetValue(object component, object value)
        {
            throw new NotSupportedException();
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        public override Type ComponentType
        { get { return typeof(DocumentView); } }

        public override bool IsReadOnly
        { get { return true; } }

        public override Type PropertyType
        {
            get
            {
                switch (column.DataType)
                {
                    case PropertyTypeEnum.Boolean:
                        return typeof(bool);
                    case PropertyTypeEnum.Integer:
                        return typeof(long);
                    case PropertyTypeEnum.Decimal:
                        return typeof(double);
                    case PropertyTypeEnum.String:
                        return typeof(string);
                    case PropertyTypeEnum.DateTime:
                        return typeof(DateTime);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
