﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

using EnterpriseManagement.Core.Entity;

namespace EnterpriseManagement.Core.UI
{
    public class GridColumnDefinition
    {
        public string Header
        { get { return header; } }

        public string Binding
        { get { return binding; } }

        public long PropertyId
        { get { return propertyId; } }

        public PropertyTypeEnum DataType
        { get { return dataType; } }

        private readonly string header;
        private readonly string binding;
        private readonly long propertyId;
        private readonly PropertyTypeEnum dataType;

        private GridColumnDefinition(string header, string binding, long propertyId, PropertyTypeEnum dataType)
        {
            if (string.IsNullOrEmpty(header))
            {
                throw new ArgumentNullException("header");
            }
            if (string.IsNullOrEmpty(header))
            {
                throw new ArgumentNullException("binding");
            }
            if (!Enum.IsDefined(typeof(PropertyTypeEnum), dataType) || dataType == PropertyTypeEnum.Unknown)
            {
                throw new ArgumentException("dataType");
            }

            this.header = header;
            this.binding = binding;
            this.dataType = dataType;
            this.propertyId = propertyId;
        }

        public GridColumnDefinition(string header, string binding, PropertyTypeEnum dataType)
            : this(header, binding, 0, dataType)
        { }

        public GridColumnDefinition(string header, long propertyId, PropertyTypeEnum dataType)
            : this(header, propertyId.ToString(CultureInfo.InvariantCulture), propertyId, dataType)
        { }

        public DataGridBoundColumn CreateColumn()
        {
            DataGridBoundColumn column;
            switch (DataType)
            {
                case PropertyTypeEnum.Boolean:
                    column = new DataGridCheckBoxColumn
                    {
                        Binding = new Binding(Binding)
                        {
                            Mode = BindingMode.OneTime,
                        }
                    };
                    break;
                case PropertyTypeEnum.Integer:
                    column = new DataGridTextColumn
                    {
                        Binding = new Binding(Binding)
                        {
                            Mode = BindingMode.OneTime,
                            StringFormat = "N0",
                        }
                    };
                    break;
                case PropertyTypeEnum.Decimal:
                    column = new DataGridTextColumn
                    {
                        Binding = new Binding(Binding)
                        {
                            Mode = BindingMode.OneTime,
                            StringFormat = "N2",
                        }
                    };
                    break;
                case PropertyTypeEnum.String:
                    column = new DataGridTextColumn
                    {
                        Binding = new Binding(Binding)
                        {
                            Mode = BindingMode.OneTime,
                        }
                    };
                    break;
                case PropertyTypeEnum.DateTime:
                    column = new DataGridTextColumn
                    {
                        Binding = new Binding(Binding)
                        {
                            Mode = BindingMode.OneTime,
                            StringFormat = "D",
                        }
                    };
                    break;
                default:
                    throw new NotSupportedException();
            }
            column.Header = Header;
            return column;
        }
    }
}
