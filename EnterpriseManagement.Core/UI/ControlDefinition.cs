﻿using System;
using System.Windows.Controls;
using System.Windows.Media;

namespace EnterpriseManagement.Core.UI
{
    public abstract class ControlDefinition
    {
        public string Name
        { get { return name; } }

        public ImageSource Icon
        { get { return icon; } }

        private readonly string name;
        private readonly ImageSource icon;

        protected ControlDefinition(string name, ImageSource icon)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            this.name = name;
            this.icon = icon;
        }

        public virtual void Show(GroupBox panel, bool fixedView)
        {
            panel.Header = name;
        }
    }
}
